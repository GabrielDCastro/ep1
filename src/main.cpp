#include<iostream>
#include<cstdlib>
#include <unistd.h>

using namespace std;

const int gridSize = 50;
void printGrid(bool gridOne[gridSize+1][gridSize+1]);
void determineState(bool gridOne[gridSize+1][gridSize+1]);


int main(){


	bool gridOne[gridSize+1][gridSize+1] = {};
	int linha, coluna, quantidade;

	char start;

	cout << "Entre com a quantidade de células!" << endl;
	cin >> quantidade;

	for(int i=0; i < quantidade; i++)
	{
		cout << "Entre com a posição da linha da célula " << i+1 << " : ";
		cin >> linha;
		cout << "Entre com a pisição da coluna da célula " << i+1 << " : ";
		cin >> coluna;
		gridOne[linha][coluna] = true;
		printGrid(gridOne);
	}

	cout << "Tudo pronto para jogar! Start the Game? (y/n)" << endl;
	printGrid(gridOne);
	cin >> start;

	if(start == 'y'|| 'Y')
	{
		while (true)
		{
								printGrid(gridOne);
            		determineState(gridOne);
            		usleep(200000);
            		system("CLS");
		}
	}
	else
	{
		return 0;
	}
}

//Método para fazer o jogo aparecer na tela
void printGrid(bool gridOne[gridSize+1][gridSize+1]){
	for(int a = 1; a < gridSize; a++)
	{
		for(int b = 1; b < gridSize; b++)
		{
			if(gridOne[a][b] == true)
			{
				//Células vivas
				cout << " O ";
			}
			else
			{
				//Células mortas
				cout << " . ";
			}
			if(b == gridSize-1)
			{
				cout << endl;
			}
		}
	}
}

//Método para comparar as células e uma das maneiras de fazer as regras do jogo funcionarem
void compareGrid (bool gridOne[gridSize+1][gridSize+1], bool gridTwo[gridSize+1][gridSize+1]){
	for(int a = 0; a < gridSize; a++)
	{
		for(int b = 0; b < gridSize; b++)
		{
			gridTwo[a][b] = gridOne[a][b];
		}
	}
}

//Determinar o estádo das células, se estarão vivas ou mortas
void determineState(bool gridOne[gridSize+1][gridSize+1]){
  bool gridTwo[gridSize+1][gridSize+1] = {};
  compareGrid(gridOne, gridTwo);
	//Maneira de fazer as regras do jogo funcionarem 
	for(int a=1; a < gridSize; a++)
	{
		for(int b = 1; b < gridSize; b++)
		{
			int alive = 0;
			for(int c = -1; c < 2; c++)
			{
				for(int d = -1; d <2; d ++)
				{
					if (!(c == 0 && d == 0))
					{
					  if(gridTwo[a+c][b+d])
					  {
						++alive;
					  }
					}
				}
			}
			if(alive < 2)
			{
			   gridOne[a][b] = false;
			}
			else if (alive == 3)
			{
			   gridOne[a][b] = true;
			}
			else if (alive > 3)
			{
			   gridOne[a][b] = false;
			}
		}
	}
}
