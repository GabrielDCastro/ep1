#ifndef GRID_HPP
#define GRID_HPP
#include <string>
#include <iostream>
#include <vector>

// using namespace std;
class Grid {
private:
	int tamanho;


public:
	Grid();
	// void setTamanho(int tamanho);


	//declaracao das funcoes publicas do grid.cpp

	//print grid recebe uma matrix de vector.
	void printGrid(int gridOne[][50]);

	//
	void compareGrid(int gridOne[][50],int gridTwo[][50]);

	//
	void determineState(int gridOne[][50]);
};

#endif
